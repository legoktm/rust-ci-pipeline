# rust-ci-pipeline

Pre-setup Gitlab-CI pipeline for your Rust projects.

There are some odd failures I'm still debugging with rust-coverage and rustdoc, so those two jobs are opt-in for now.

Example:
```yaml
include:
  - 'https://gitlab.com/legoktm/rust-ci-pipeline/-/raw/master/jobs.yml'

variables:
  APT_PACKAGES: libssl-dev
  DNF_PACKAGES: openssl-devel
  CARGO_AUDIT: --ignore RUSTSEC-2020-0016
  SKIP_COVERAGE: "yes"

rust-coverage:
  extends: .rust-coverage
  allow_failure: true

pages:
  extends: .pages
```
